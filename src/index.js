// imports
const sulla = require('sulla');
const axios = require('axios');
const buscaCep = require('busca-cep');
const moment = require('moment');

// criação da instância WhatsApp
sulla.create('pedidos').then((client) => start(client));

// Busca do Menu
getMenu();

let customerOrders = {};

// Definição das palavras padrão
const negativeWords = ['NAO',"NEGATIVO",'NÃO',"NN","N",'NUNCA','NO'];
const positiveWords = ['SIM',"POSITIVO",'S',"SS",'CLARO','OKAY','OK','YES'];
const greetingSentences = [' ','BOM DIA', 'OI','OLA','BOA TARDE','BOA NOITE','TUDO BEM','?','SALVE','ALO',"TUDO BOM",];

// Cardápio
let menu = [];
let itemCodToProduct = [];
let products = {};
let menuText = '';

// Icone de cada categoria
const groupIcon = {
    'BEBIDAS' : '🥤',
    'PRATOS' : '🥣',
    'LANCHES' : '🥪',
    'FRUTAS' : '🍎',
}
function greetingPeriod(){   
    period = moment().format('LT').split(' ');
    hour = period[0].split(':')
    if (period[1] == 'AM'){
        return "Bom dia!";
    }
    else if(period[1] == 'PM') {
        if(Number(hour[0])>=6){
            return "Boa Noite!";
        }
        else if(Number(hour[0])<=6){
            return "Boa Tarde!";
        }        
    } 
}
function start(client) {

    // Controlador de mensagens
    client.onMessage((message) => {
        const customers = Object.keys(customerOrders);
        console.log(message)
        if (!customers.includes(message.from)) {

            customerOrders[message.from] = {
                name: message.sender.pushname,
                phone: message.sender.formattedName,
                currentStep: 'greeting',
                orderItems: [],
                cep: '',
                cepJson: {},
                payment: '',
                cash: '',
                fullAddress: ''
            };

            console.log(customerOrders);
        }

        // Mensagem inicial
        if (greetingSentences.includes(message.body.toUpperCase()) && customerOrders[message.from]['currentStep'] === 'greeting'){
            greeting = greetingPeriod()
            client.sendText(message.from,greeting);
            client.sendText(message.from,"Sou a assistente virtual do Restaurante Odhen");
            client.sendText(message.from,"Vamos fazer um pedido?");
            customerOrders[message.from]['currentStep'] = 'startOrder';

        }

        // Mensagem cancelamento
        if (message.body.toUpperCase() === "CANCELAR"){

            // Cancelamento do processo
            client.sendText(message.from,"Okay. Seu pedido foi cancelado.");
            client.sendText(message.from,"Quando quiser fazer um pedido é só me chamar.");

            customerOrders[message.from]['currentStep'] = 'greeting';
        }

        // Início do processo
        if (customerOrders[message.from]['currentStep'] === 'startOrder'){

            // Questiona se deseja fazer um novo pedido
           if (positiveWords.includes(message.body.toUpperCase())){
                client.sendText(message.from,"Para cancelar o pedido a qualquer momento\nEnvie 'Cancelar'");
                client.sendText(message.from,"Deseja ver o cardapio?");
                customerOrders[message.from]['currentStep'] = 'viewMenu';

           } else if(negativeWords.includes(message.body.toUpperCase())){

                client.sendText(message.from,"Okay.");
                client.sendText(message.from,"Quando quiser fazer um pedido é só me chamar.");

                customerOrders[message.from]['currentStep'] = 'greeting';
           }
        } else if (customerOrders[message.from]['currentStep'] === 'viewMenu'){

            if (positiveWords.includes(message.body.toUpperCase())) {

                sendMenu(client,message);
                client.sendText(message.from,"Para *reiniciar* a escolha dos itens, envie 'Reiniciar'");                
                customerOrders[message.from]['currentStep'] = 'makeOrder';

            }
            else if(negativeWords.includes(message.body.toUpperCase())) {

                client.sendText(message.from,"Para pedir, envie o código produto desejado. Envie um por vez e apenas o número.");
                client.sendText(message.from,"Para *finalizar* a escolha, basta enviar 'Finalizar'");
                client.sendText(message.from,"Para *reiniciar* a escolha dos itens, envie 'Reiniciar'");
                customerOrders[message.from]['currentStep'] = 'makeOrder';

            }

        }
        else if (customerOrders[message.from]['currentStep'] === 'makeOrder'){

            if (message.body.toUpperCase() === 'FINALIZAR' && customerOrders[message.from]['orderItems'].length>0){


                mountedItensList = "🍽 *Seu pedido:*\n\n";
                totalValue = 0;
                customerOrders[message.from]['orderItems'].forEach(item => {

                    mountedItensList +="• "+ item.itemName +  " - R$ " + item.itemValue+"\n";
                    totalValue += item.itemValue;
                });
                mountedItensList +="\n";
                mountedItensList +="  Valor total do pedido: R$"+ totalValue +" \n";
                client.sendText(message.from,mountedItensList);
                client.sendText(message.from,"Você irá pagar no dinheiro ou no cartão?");
                client.sendText(message.from,"Aceitamos as seguintes bandeiras (em crédito ou débito): Visa, MasterCard, Elo e American Express.\nNão aceitamos Vale Refeição e nem Vale Alimentação.");

                customerOrders[message.from]['currentStep'] = 'setPayment';

            } else if (message.body.toUpperCase() === 'FINALIZAR' && customerOrders[message.from]['orderItems'].length==0){

                client.sendText(message.from,"Para finalizar um pedido, escolha pelo menos um item.");

            }else if (message.body.toUpperCase() === 'REINICIAR'&& customerOrders[message.from]['orderItems'].length>0){
               
                while (customerOrders[message.from]['orderItems'].length>0) {
                    customerOrders[message.from]['orderItems'].pop();
                    
                    
                }                                   
                client.sendText(message.from,"Pode escolher os itens novamente");
            }
            else {
              // Transfere o código para inteiro para verificar se é um código valido
                const codeSent = parseInt(message.body, 10);
                
                if(isNaN(codeSent)) {

                    client.sendText(message.from,"Envie apenas o número do item que você deseja.");

                } else if (itemCodToProduct[codeSent] == null) {

                    client.sendText(message.from,"Item não presente no cardapio.");

                }
                else {

                    const product = products[itemCodToProduct[codeSent]];
                    customerOrders[message.from]['orderItems'].push(product);

                    const total = getTotalOrder(customerOrders[message.from]['orderItems']);

                    client.sendText(message.from,product.itemName + " adicionado.");
                    client.sendText(message.from,"Valor total do pedido: R$ " + total);

                }
          }
        } else if (customerOrders[message.from]['currentStep'] === 'setPayment') {

            if (message.body.toUpperCase() === 'DINHEIRO') {

                customerOrders[message.from]['paymentId'] = 'DIN';
                client.sendText(message.from,"💵 Vai precisar de troco para quanto? Se não precisar, digite 'não'");
                customerOrders[message.from]['currentStep'] = 'setCash';

            } else if (message.body.toUpperCase() === 'CARTÃO' || message.body.toUpperCase() === 'CARTAO') {

                customerOrders[message.from]['paymentId'] = 'CAR';
                customerOrders[message.from]['payment'] = message.body;
                client.sendText(message.from,"Qual é o cep da entrega? (apenas números)");
                customerOrders[message.from]['currentStep'] = 'setCep';

            } else {
                client.sendText(message.from,"Digite cartão ou dinheiro.");
            }

        } else if (customerOrders[message.from]['currentStep'] === 'setCash') {

            customerOrders[message.from]['cash'] = message.body;
            client.sendText(message.from, "Ok! Anotei aqui. 😃");
            client.sendText(message.from,"Qual é o cep da entrega? (apenas números)");
            customerOrders[message.from]['currentStep'] = 'setCep';

        } else if (customerOrders[message.from]['currentStep'] === 'setCep'){

            if (message.body.length === 8) {

                const cepJson = buscaCep(message.body, { sync: true});

                if (!cepJson.hasError && cepJson.bairro != undefined) {

                    customerOrders[message.from]['cepJson'] = cepJson;

                    customerOrders[message.from]['cep'] = message.body;

                    const fullAddress = cepJson.logradouro + "\n" + cepJson.bairro + " - " + cepJson.localidade + "/" + cepJson.uf;
                    customerOrders[message.from]['fullAddress'] = fullAddress;
                    customerOrders[message.from]['street'] = cepJson.logradouro + " " + cepJson.bairro;

                    client.sendText(message.from,"Localizei aqui: " + fullAddress);
                    client.sendText(message.from,"Agora me informe o número e, se houver, o bloco e apartamento. Exemplo: Número 1000, Bloco 2, Apto 101 ");

                    customerOrders[message.from]['currentStep'] = 'setAddressNumber';

                } else {

                    client.sendText(message.from,"CEP não encontrado ou incorreto");

                }

            } else {

                client.sendText(message.from,"Digite seu CEP com apenas números.");

            }

        } else if(customerOrders[message.from]['currentStep'] === 'setAddressNumber'){

            const fullAddress = customerOrders[message.from]['fullAddress'] + " -> " + message.body;
            customerOrders[message.from]['fullAddress'] = fullAddress;
            customerOrders[message.from]['number'] = message.body;

            client.sendText(message.from,"Beleza! \n🛵 Vou entregar em:");
            client.sendText(message.from, fullAddress);

            mountedItensList = "🧾 *Itens:*\n\n";
            customerOrders[message.from]['orderItems'].forEach(item => {
                mountedItensList +="• "+ item.itemName+"\n";
            });
            client.sendText(message.from,mountedItensList);
            client.sendText(message.from,"Posso confirmar seu pedido?");

            customerOrders[message.from]['currentStep'] = 'confirmation';

        } else if(customerOrders[message.from]['currentStep'] === 'confirmation'){

            if (positiveWords.includes(message.body.toUpperCase())){

                sendOrder(customerOrders[message.from], client, message);

                client.sendText(message.from,"Seu pedido foi CONFIRMADO. Em breve irei te atualizar quanto ao status dele.");
                client.sendText(message.from,"Obrigado!! 💚 ");
                client.sendImageAsSticker(message.from, 'C:\\Users\\Ian Rafael\\Documents\\teknisa\\chatbot\\chatbot\\src\\lib\\anotei.jpg');
            } else if(negativeWords.includes(message.body.toUpperCase())){

                client.sendText(message.from,"Okay. 😢");
                client.sendText(message.from,"Seu pedido foi cancelado. Para reiniciar, basta enviar um Oi :)");

            } else {

                client.sendText(message.from,"Digite apenas *sim* ou *não*.");

            }

        }
    });
}

function mountMenu() {
    let i = 1;

    let menuTextComplete = '📜 Cardápio:\n\n';

    menu.forEach((group) => {
        menuTextComplete += ' '+ groupIcon[group.groupName] +' *' + group.groupName + '*\n \n';
         
        group.products.forEach((product) => {
            itemCodToProduct[i] = product.itemId;
            products[product.itemId] = product;
            menuTextComplete += '[' + i + '] - ' + product.itemName + ' - R$' + product.itemValue + '\n';
            i++;
        });

        menuTextComplete += '\n';
    });

    return menuTextComplete;
}

function getMenu() {

    const params = {
        "branchId": "0024"
    };
    const options = {
        headers: {
            "Authorization": "bearer 6FaCybiTBOWlcOn46eoPN04Bnd2hch4fNuivPiDrO+1NVro7bz4N4+/X7Ey9MRT8NmUtcJ4hUIfZfFCRGAzQvA==",
            "Content-Type": "application/json"
        }
    };

    axios.post('http://briansilva1.zeedhi.com/workfolder/odhen-orderapi/backend/service/index.php/menu', params, options)
        .then(function (response) {

            if (response.data.success) {
                menu = response.data.menu.Products;
                menuText = mountMenu();
            } else {
                console.log('[#] Erro ao carregar menu.')
            }

        })
        .catch(function (error) {
            console.log(error);
        });
}

function sendMenu(client,message) {

    client.sendText(message.from, menuText);
    client.sendText(message.from,"Para pedir, envie o código a frente do produto desejado. Envie um por vez e apenas o número.");
    client.sendText(message.from,"Para *finalizar* a escolha, basta enviar 'Finalizar'");
}

function sendOrder(order, client, message){

    const options = {
        headers: {
            "Authorization": "bearer 6FaCybiTBOWlcOn46eoPN04Bnd2hch4fNuivPiDrO+1NVro7bz4N4+/X7Ey9MRT8NmUtcJ4hUIfZfFCRGAzQvA==",
            "Content-Type": "application/json"
        }
    };

    const params = mountSale(order);
    console.log(params);

    axios.post('http://briansilva1.zeedhi.com/workfolder/odhen-orderapi/backend/service/index.php/order', params, options)
        .then(function (response) {

            client.sendText(message.from,"Seu pedido foi integrado com sucesso.");
            console.log(response.data);
            const dados = response.data.result['Order'].split("-");
            client.sendText(message.from,"CDFILIAL: " + dados[0]);
            client.sendText(message.from,"NRCOMANDA: " + dados[1]);
            client.sendText(message.from,"NRVENDAREST: " + dados[2]);
            client.sendText(message.from,"NRCOMANDAEXT: " + dados[3]);
            client.sendText(message.from,"Dados para fins de debbugin. Não serão enviados em produção.");
            delete customerOrders[message.from];

        })
        .catch(function (error) {
            console.log(error);
        });
}



function getTotalOrder(orderList) {

    let total = 0;

    orderList.forEach((item) => {
        total += item.itemValue
    });

    return total;

}

function mountSale(order) {

    const total = getTotalOrder(order.orderItems);

    const itemOrder = [];

    order.orderItems.forEach((item) => {
        item = {
            "title": item.itemName,
            "quantity": 1,
            "price": item.itemValue,
            "total": item.itemValue,
            "notes": "",
            "ref": item.itemId,
            "ComplementCategories": []
        };

        itemOrder.push(item);
    });

    const date = moment().format('YYYY-MM-DD HH:mm:ss');

    return {
        "saleId": "001",
        "orderNumber": "1004",
        "orderType": 1,
        "origin": "API01",
        "date": date,
        "scheduleDate": date,
        "subsidiaryCode": "0024",
        "tax": 0,
        "subTotal": total,
        "total": total,
        "troco": 0,
        "discount": 0,
        "latitude": 0.0,
        "longitude": 0.0,
        "cpfInNote": "",
        "notes": "",
        "consumerDetail": {
            "clientId": "562122",
            "name": order.name,
            "email": "clienteTeste26@gmail.com",
            "phone": order.phone,
            "cellPhone": "",
            "cpf": "",
            "address": {
                "zipCode": order.cep,
                "street": order.street,
                "number": order.number,
                "neighborhood": order.cepJson.bairro,
                "city": order.cepJson.localidade,
                "uf": order.cepJson.uf,
                "complement": "",
                "referencePoint": ""
            }
        },
        "payments": [
            {
                "paymentId": "DIN",
                "value": total
            }
        ],
        "ItemOrder": itemOrder
    }
}
